package com.androidegitim.fotograf;


import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.androidegitim.androidegitimlibrary.helpers.RDALogger;
import com.frosquivel.magicalcamera.MagicalCamera;
import com.frosquivel.magicalcamera.MagicalPermissions;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

//https://github.com/fabian7593/MagicalCamera
public class MainActivity extends AppCompatActivity {

    @BindView(R.id.main_imageview)
    ImageView imageView;

    private int RESIZE_PHOTO_PIXELS_PERCENTAGE = 50;

    private MagicalPermissions magicalPermissions;

    private MagicalCamera magicalCamera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        RDALogger.start(getString(R.string.app_name)).enableLogging(true);

        String[] permissions = new String[]{
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        };

        magicalPermissions = new MagicalPermissions(this, permissions);

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                Toast.makeText(MainActivity.this, "İzin verdiğiniz için teşekkürler", Toast.LENGTH_LONG).show();
            }
        };

        magicalPermissions.askPermissions(runnable);

        magicalCamera = new MagicalCamera(this, RESIZE_PHOTO_PIXELS_PERCENTAGE, magicalPermissions);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Map<String, Boolean> map = magicalPermissions.permissionResult(requestCode, permissions, grantResults);
        for (String permission : map.keySet()) {
            Log.d("PERMISSIONS", permission + " was: " + map.get(permission));
        }
        //Following the example you could also
        //locationPermissions(requestCode, permissions, grantResults);
    }

    @OnClick(R.id.main_button_take_picture)
    public void takePicture() {

        magicalCamera.takePhoto();
    }

    @OnClick(R.id.main_button_choose_from_gallery)
    public void chooseFromGallery() {

        magicalCamera.selectedPicture("resim seçmek için uygulama seçin");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        
        magicalCamera.resultPhoto(requestCode, resultCode, data, MagicalCamera.ORIENTATION_ROTATE_90);

        imageView.setImageBitmap(magicalCamera.getPhoto());

        String path = magicalCamera.savePhotoInMemoryDevice(magicalCamera.getPhoto(), "myPhotoName", "myDirectoryName", MagicalCamera.JPEG, true);

        RDALogger.start("DOSYA YOLU : " + path);

        if (path != null) {
            Toast.makeText(MainActivity.this, "Fotoğraf kaydedildi, dosya yolu: " + path, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(MainActivity.this, "Fotoğraf kaydedilemedi.", Toast.LENGTH_SHORT).show();
        }
    }
}
